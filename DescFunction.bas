Attribute VB_Name = "DescFunction"
Public Declare Function TrocarCodigoDeAtivacao Lib "dllsat.dll" (ByVal numSessao As Long, ByVal codAtivacao As String, ByVal opcao As Long, ByVal novoCod As String, ByVal confNovoCod As String) As Long
Public Declare Function ConsultarSAT Lib "dllsat.dll" (ByVal numSessao As Long) As Long
Public Declare Function EnviarDadosVenda Lib "dllsat.dll" (ByVal numSessao As Long, ByVal codAtivacao As String, ByVal dadosVenda As String) As Long
Public Declare Function CancelarUltimaVenda Lib "dllsat.dll" (ByVal numSessao As Long, ByVal codAtivacao As String, ByVal chave As String, ByVal dadosCancela As String) As Long
Public Declare Function TesteFimAFim Lib "dllsat.dll" (ByVal numSessao As Long, ByVal codAtivacao As String, ByVal dadosVendas As String) As Long
Public Declare Function ConsultarStatusOperacional Lib "dllsat.dll" (ByVal numSessao As Long, ByVal codAtivacao As String) As Long
Public Declare Function ConsultarNumeroSessao Lib "dllsat.dll" (ByVal numSessao As Long, ByVal codAtivacao As String, ByVal nSessao As Long) As Long
Public Declare Function ConfigurarInterfaceDeRede Lib "dllsat.dll" (ByVal numSessao As Long, ByVal codAtivacao As String, ByVal configuracao As String) As Long
Public Declare Function AssociarAssinatura Lib "dllsat.dll" (ByVal numSessao As Long, ByVal codAtivacao As String, ByVal CNPJVal As String, ByVal AssCNPJ As String) As Long
Public Declare Function AtualizarSoftwareSAT Lib "dllsat.dll" (ByVal numSessao As Long, ByVal codAtivação As String) As Long
Public Declare Function ExtrairLogs Lib "dllsat.dll" (ByVal numSessao As Long, ByVal codAtivacao As String) As Long
Public Declare Function BloquearSAT Lib "dllsat.dll" (ByVal numSessao As Long, ByVal codAtivacao As String) As Long
Public Declare Function DesbloquearSAT Lib "dllsat.dll" (ByVal numSessao As Long, ByVal codAtivacao As String) As Long
Public Declare Function GeraNumeroSessao Lib "dllsat.dll" () As Long
Public Declare Function AtivarSAT Lib "dllsat.dll" (ByVal numSessao As Long, ByVal subCommand As Long, ByVal codAtivacao As String, ByVal CNPJCont As String, ByVal cUF As String) As Long


Public Declare Function lstrlenW Lib "kernel32" (ByVal lpString As Long) As Long
Public Declare Function lstrlenA Lib "kernel32" (ByVal lpString As Long) As Long
Public Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (destination As Any, Source As Any, ByVal Length As Long)


Function PointerToString(lngPtr As Long) As String
   Dim sBuffer As String
   Dim lStringLen As Long

  lStringLen = lstrlenW(lngPtr) * 2
 If lStringLen > 0 Then
        sBuffer = Space(lStringLen)
        CopyMemory ByVal sBuffer, ByVal lngPtr, lStringLen
       PointerToString = Replace(sBuffer, Chr(0), "")
  End If
End Function

