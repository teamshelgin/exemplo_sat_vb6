VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form Form1 
   Caption         =   "SAT - Elgin"
   ClientHeight    =   8220
   ClientLeft      =   165
   ClientTop       =   555
   ClientWidth     =   9705
   LinkTopic       =   "Form1"
   ScaleHeight     =   8220
   ScaleWidth      =   9705
   StartUpPosition =   3  'Windows Default
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   5400
      Top             =   7200
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Frame Frame2 
      Caption         =   "Retorno"
      Height          =   6855
      Left            =   5400
      TabIndex        =   18
      Top             =   240
      Width           =   5655
      Begin VB.TextBox Text1 
         Height          =   6015
         Left            =   360
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   19
         Top             =   600
         Width           =   5055
      End
   End
   Begin VB.CommandButton Command14 
      Caption         =   "Trocar Codigo Ativa��o"
      Height          =   495
      Left            =   480
      TabIndex        =   17
      Top             =   6720
      Width           =   2175
   End
   Begin VB.CommandButton Command13 
      Caption         =   "Desbloquear SAT"
      Height          =   495
      Left            =   2880
      TabIndex        =   13
      Top             =   6720
      Width           =   2175
   End
   Begin VB.CommandButton Command12 
      Caption         =   "Bloquear SAT"
      Height          =   495
      Left            =   2880
      TabIndex        =   12
      Top             =   6120
      Width           =   2175
   End
   Begin VB.CommandButton Command11 
      Caption         =   "Extrair Logs"
      Height          =   495
      Left            =   2880
      TabIndex        =   11
      Top             =   5520
      Width           =   2175
   End
   Begin VB.CommandButton Command10 
      Caption         =   "Atualizar Software SAT"
      Height          =   495
      Left            =   2880
      TabIndex        =   10
      Top             =   4920
      Width           =   2175
   End
   Begin VB.CommandButton Command9 
      Caption         =   "Associar Assinatura"
      Height          =   495
      Left            =   2880
      TabIndex        =   9
      Top             =   4320
      Width           =   2175
   End
   Begin VB.CommandButton Command8 
      Caption         =   "Configurar Interface de Rede"
      Height          =   495
      Left            =   2880
      TabIndex        =   8
      Top             =   3720
      Width           =   2175
   End
   Begin VB.CommandButton Command7 
      Caption         =   "Consultar Numero de Sess�o"
      Height          =   495
      Left            =   2880
      TabIndex        =   7
      Top             =   3120
      Width           =   2175
   End
   Begin VB.CommandButton Command6 
      Caption         =   "Consultar Status Operacional"
      Height          =   495
      Left            =   480
      TabIndex        =   6
      Top             =   6120
      Width           =   2175
   End
   Begin VB.CommandButton Command5 
      Caption         =   "Teste Fim a Fim"
      Height          =   495
      Left            =   480
      TabIndex        =   5
      Top             =   5520
      Width           =   2175
   End
   Begin VB.CommandButton Command4 
      Caption         =   "Cancelar Venda"
      Height          =   495
      Left            =   480
      TabIndex        =   4
      Top             =   4920
      Width           =   2175
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Efetuar Venda"
      Height          =   495
      Left            =   480
      TabIndex        =   3
      Top             =   4320
      Width           =   2175
   End
   Begin VB.TextBox Text2 
      Height          =   285
      Left            =   2520
      TabIndex        =   2
      Text            =   "123456789"
      Top             =   600
      Width           =   2175
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Ativar SAT"
      Height          =   495
      Left            =   480
      TabIndex        =   1
      Top             =   3720
      Width           =   2175
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Consultar SAT"
      Height          =   495
      Left            =   480
      TabIndex        =   0
      Top             =   3120
      Width           =   2175
   End
   Begin VB.Frame Frame1 
      Caption         =   "Dados"
      Height          =   2775
      Left            =   480
      TabIndex        =   15
      Top             =   240
      Width           =   4575
      Begin VB.TextBox Text6 
         Height          =   285
         Left            =   2040
         TabIndex        =   30
         Top             =   2160
         Width           =   1815
      End
      Begin VB.CommandButton Command15 
         Caption         =   "..."
         Height          =   255
         Left            =   3840
         TabIndex        =   29
         Top             =   2160
         Width           =   375
      End
      Begin VB.TextBox Text5 
         Height          =   285
         Left            =   2040
         TabIndex        =   27
         Text            =   "14200166000166"
         Top             =   1080
         Width           =   2175
      End
      Begin VB.TextBox Text4 
         Height          =   285
         Left            =   2040
         TabIndex        =   24
         Text            =   "SGR-SAT SISTEMA DE GESTAO E RETAGUARDA DO SAT"
         Top             =   1440
         Width           =   2175
      End
      Begin VB.ComboBox Combo1 
         Height          =   315
         ItemData        =   "Form1.frx":0000
         Left            =   2040
         List            =   "Form1.frx":0055
         TabIndex        =   23
         Text            =   "SP"
         Top             =   1800
         Width           =   2175
      End
      Begin VB.TextBox Text3 
         Height          =   285
         Left            =   2040
         TabIndex        =   21
         Text            =   "14200166000166"
         Top             =   720
         Width           =   2175
      End
      Begin VB.Label Label8 
         Caption         =   "Selecionar arquivo:"
         Height          =   255
         Left            =   240
         TabIndex        =   28
         Top             =   2160
         Width           =   1695
      End
      Begin VB.Label Label7 
         Caption         =   "CNPJ Contribuinte:"
         Height          =   255
         Left            =   240
         TabIndex        =   26
         Top             =   1080
         Width           =   1695
      End
      Begin VB.Label Label6 
         Caption         =   "Codigo de vincula��o:"
         Height          =   255
         Left            =   240
         TabIndex        =   25
         Top             =   1440
         Width           =   1695
      End
      Begin VB.Label Label5 
         Caption         =   "UF: "
         Height          =   255
         Left            =   240
         TabIndex        =   22
         Top             =   1800
         Width           =   1575
      End
      Begin VB.Label Label4 
         Caption         =   "CNPJ Softhouse:"
         Height          =   255
         Left            =   240
         TabIndex        =   20
         Top             =   720
         Width           =   1455
      End
      Begin VB.Label Label1 
         Caption         =   "Codigo de Ativa��o:"
         Height          =   255
         Left            =   240
         TabIndex        =   16
         Top             =   360
         Width           =   1815
      End
   End
   Begin VB.Line Line1 
      X1              =   0
      X2              =   11280
      Y1              =   7800
      Y2              =   7800
   End
   Begin VB.Label Label2 
      Caption         =   "Desenvolvidor: Bruno Cruz  |  Em : 17/10/2016  |  E-mail: bruno.cruz@elgin.com  |  Empresa: Elgin  |  Site: www.elgin.com.br"
      ForeColor       =   &H8000000C&
      Height          =   255
      Left            =   1200
      TabIndex        =   14
      Top             =   7920
      Width           =   9255
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim ret As String
Dim codA As String
Dim i As Long
Dim retorno As String

Private Sub Command1_Click()

i = GeraNumeroSessao
i = ConsultarSAT(i)
ret = PointerToString(i)
Text1.Text = ret
End Sub

Private Sub Command10_Click()
If Text2.Text = "" Then
    MsgBox "Preencha o Codigo de ativa��o", vbCritical, "Erro"
Exit Sub
End If

codA = Text2.Text
i = GeraNumeroSessao

i = AtualizarSoftwareSAT(i, codA)
ret = PointerToString(i)
Text1.Text = ret

End Sub

Private Sub Command11_Click()

If Text2.Text = "" Then
    MsgBox "Preencha o Codigo de ativa��o", vbCritical, "Erro"
Exit Sub
End If

Dim val() As String
codA = Text2.Text
i = GeraNumeroSessao
i = ExtrairLogs(i, codA)
ret = PointerToString(i)
val = Split(ret, "|")
Text1.Text = ""
Text1.Text = "Numero Sessao: " & val(0)
Text1.Text = Text1.Text & vbCrLf & "Codigo Retorno: " & val(1)
Text1.Text = Text1.Text & vbCrLf & "Mensagem: " & val(2)
Text1.Text = Text1.Text & vbCrLf & "Codigo de Referencia: " & val(3)
Text1.Text = Text1.Text & vbCrLf & "Mensagem Sefaz: " & val(4)
retorno = val(5)
Text1.Text = Text1.Text & vbCrLf & "Arquivo Base64: " & val(5) 'Replace(Base64DecodeString(val(5)), "2016", vbCrLf & "2016")
End Sub

Private Sub Command12_Click()
If Text2.Text = "" Then
    MsgBox "Preencha o Codigo de ativa��o", vbCritical, "Erro"
Exit Sub
End If

i = GeraNumeroSessao
codA = Text2.Text
i = BloquearSAT(i, codA)
ret = PointerToString(i)
Text1.Text = ""
Text1.Text = ret
End Sub

Private Sub Command13_Click()
If Text2.Text = "" Then
    MsgBox "Preencha o Codigo de ativa��o", vbCritical, "Erro"
Exit Sub
End If

i = GeraNumeroSessao
codA = Text2.Text

i = DesbloquearSAT(i, codA)
ret = PointerToString(i)
Text1.Text = ""
Text1.Text = ret

End Sub

Private Sub Command14_Click()
If Text2.Text = "" Then
    MsgBox "Preencha o Codigo de ativa��o", vbCritical, "Erro"
Exit Sub
End If

Dim val() As String
Dim op As String
codA = Text2.Text
i = GeraNumeroSessao

op = InputBox("Informe a op��o do codigo de ativa��o. 1 para Codigo de Ativa��o ou 2 para codigo de ativa��o de emergencia", "Trocar codigo de ativa��o")

newcod = InputBox("Informe o novo codigo de ativa��o (recomendado 9 digitos num�ricos)", "Codigo de ativa��o")
confCod = InputBox("Informe novamente o  codigo de ativa��o (recomendado 9 digitos num�ricos)", "Confirma��o do codigo de ativa��o")

i = TrocarCodigoDeAtivacao(i, codA, op, newcod, confCod)
ret = PointerToString(i)
val = Split(ret, "|")
Text1.Text = "Numero de sess�o: " & vbCrLf & val(0) & vbCrLf & vbCrLf
Text1.Text = Text1.Text & "Codigo: " & vbCrLf & val(1) & vbCrLf & vbCrLf
Text1.Text = Text1.Text & "Mensagem: " & vbCrLf & val(2) & vbCrLf & vbCrLf
Text1.Text = Text1.Text & "Cod. Ref. Mensagem SEFAZ: " & vbCrLf & val(3) & vbCrLf & vbCrLf
Text1.Text = Text1.Text & "Mensagem SEFAZ: " & vbCrLf & val(4) & vbCrLf & vbCrLf

End Sub

Private Sub Command15_Click()
Dim filter As String

CommonDialog1.CancelError = True

On Error GoTo Erro

filter = "Todos os Arqs. (*.xml) | *.xml"
CommonDialog1.filter = filter
CommonDialog1.DefaultExt = "*.xml"

CommonDialog1.Flags = cdlOFNHideReadOnly
CommonDialog1.ShowOpen

Text6.Text = CommonDialog1.FileName
Exit Sub
 
Erro:
MsgBox " Ocorreu um erro durante a carga do arquivo !", vbCritical

End Sub


Private Sub Command3_Click()
If Text2.Text = "" Then
    MsgBox "Preencha o Codigo de ativa��o", vbCritical, "Erro"
Exit Sub
End If
If Text6.Text = "" Then
    MsgBox "Selecione o arquivo XML", vbCritical, "Erro"
Exit Sub
End If

Dim val() As String
Dim dados As String
Dim xml As DOMDocument60
Set xml = New DOMDocument60
xml.Load (Text6.Text)
dados = xml.xml

i = GeraNumeroSessao
codA = Text2.Text

Text1.Text = ""
Text1.Text = dados

i = EnviarDadosVenda(i, codA, dados)
ret = PointerToString(i)
MsgBox ret

val = Split(ret, "|")
Text1.Text = "Numero de sess�o: " & vbCrLf & val(0) & vbCrLf & vbCrLf
Text1.Text = Text1.Text & "Codigo: " & vbCrLf & val(1) & vbCrLf & vbCrLf
Text1.Text = Text1.Text & "Codigo de Alerta: " & vbCrLf & val(2) & vbCrLf & vbCrLf
Text1.Text = Text1.Text & "Mensagem: " & vbCrLf & val(3) & vbCrLf & vbCrLf
Text1.Text = Text1.Text & "Cod. Ref. Mensagem SEFAZ: " & vbCrLf & val(4) & vbCrLf & vbCrLf
Text1.Text = Text1.Text & "Mensagem SEFAZ: " & vbCrLf & val(5) & vbCrLf & vbCrLf
Text1.Text = Text1.Text & "Arq. Base64: " & vbCrLf & val(6) & vbCrLf & vbCrLf
Text1.Text = Text1.Text & "TimerStamp: " & vbCrLf & val(7) & vbCrLf & vbCrLf
Text1.Text = Text1.Text & "Chave: " & vbCrLf & val(8) & vbCrLf & vbCrLf
Text1.Text = Text1.Text & "Valor total CFe: " & vbCrLf & val(9) & vbCrLf & vbCrLf
Text1.Text = Text1.Text & "CPFCNPJ: " & vbCrLf & val(10) & vbCrLf & vbCrLf
Text1.Text = Text1.Text & "Assinatura QRCode: " & vbCrLf & val(11)

End Sub

Private Sub Command4_Click()
If Text2.Text = "" Then
    MsgBox "Preencha o Codigo de ativa��o", vbCritical, "Erro"
Exit Sub
End If
If Text6.Text = "" Then
    MsgBox "Selecione o arquivo XML", vbCritical, "Erro"
Exit Sub
End If


Dim chave As String
Dim val() As String
Dim dados As String
Dim xml As DOMDocument60

Set xml = New DOMDocument60
xml.Load (Text6.Text)
dados = xml.xml

chave = InputBox("Informe a chave do cupom", "Cancelar venda")

If chave = "" Then
    MsgBox "Necess�rio informa a chave o cupom", vbCritical, "Aten��o!"
Exit Sub
Else
If Len(chave) <> 47 Then
MsgBox "Chave do cupom invalida", vbCritical, "Aten��o!"
Exit Sub
End If
End If

i = GeraNumeroSessao
codA = Text2.Text

MsgBox i & " >< " & codA & dados
i = CancelarUltimaVenda(i, codA, chave, dados)
ret = PointerToString(i)
MsgBox ret

val = Split(ret, "|")
Text1.Text = "Numero de sess�o: " & vbCrLf & val(0) & vbCrLf & vbCrLf
Text1.Text = Text1.Text & "Codigo: " & vbCrLf & val(1) & vbCrLf & vbCrLf
Text1.Text = Text1.Text & "Codigo de Alerta: " & vbCrLf & val(2) & vbCrLf & vbCrLf
Text1.Text = Text1.Text & "Mensagem: " & vbCrLf & val(3) & vbCrLf & vbCrLf
Text1.Text = Text1.Text & "Cod. Ref. Mensagem SEFAZ: " & vbCrLf & val(4) & vbCrLf & vbCrLf
Text1.Text = Text1.Text & "Mensagem SEFAZ: " & vbCrLf & val(5) & vbCrLf & vbCrLf
Text1.Text = Text1.Text & "Arq. Base64: " & vbCrLf & val(6) & vbCrLf & vbCrLf
Text1.Text = Text1.Text & "TimerStamp: " & vbCrLf & val(7) & vbCrLf & vbCrLf
Text1.Text = Text1.Text & "Chave: " & vbCrLf & val(8) & vbCrLf & vbCrLf
Text1.Text = Text1.Text & "Valor total CFe: " & vbCrLf & val(9) & vbCrLf & vbCrLf
Text1.Text = Text1.Text & "CPFCNPJ: " & vbCrLf & val(10) & vbCrLf & vbCrLf
Text1.Text = Text1.Text & "Assinatura QRCode: " & vbCrLf & val(11)

End Sub

Private Sub Command5_Click()
If Text2.Text = "" Then
    MsgBox "Preencha o Codigo de ativa��o", vbCritical, "Erro"
Exit Sub
End If
If Text3.Text = "" Then
    MsgBox "Preencha o campo CNPJ Softhouse.", vbCritical, "Erro"
Exit Sub
End If
If Text5.Text = "" Then
    MsgBox "Preencha o campo CNPJ Contribuinte", vbCritical, "Erro"
Exit Sub
End If
If Text4.Text = "" Then
    MsgBox "Preencha o campo Codigo de vincula��o", vbCritical, "Erro"
Exit Sub
End If

Dim dadosVenda As String
Dim val() As String
codA = Text2.Text
i = GeraNumeroSessao

dadosVenda = "<CFe><infCFe versaoDadosEnt=" & Chr(34) & "0.06" & Chr(34) & "><ide><CNPJ>" _
& Text3.Text & "</CNPJ><signAC>" & Text4.Text & "</signAC>" _
& "<numeroCaixa>001</numeroCaixa></ide><emit><CNPJ>" & Text5.Text _
& "</CNPJ><IE>111111111111</IE><IM>111111</IM><indRatISSQN>N</indRatISSQN>" _
& "</emit><dest/><det nItem=" & Chr(34) & "1" & Chr(34) & "><prod><cProd>00000000000017</cProd>" _
& "<cEAN>00000000000017</cEAN><xProd>Produto teste</xProd><CFOP>5102</CFOP>" _
& "<uCom>KG</uCom><qCom>1.0000</qCom><vUnCom>1.00</vUnCom><indRegra>A</indRegra>" _
& "<obsFiscoDet xCampoDet=" & Chr(34) & "xCampoDet" & Chr(34) & "><xTextoDet>xTextoDet</xTextoDet></obsFiscoDet>" _
& "</prod><imposto><vItem12741>0.30</vItem12741><ICMS><ICMS00><Orig>0</Orig><CST>00</CST>" _
& "<pICMS>7.00</pICMS></ICMS00></ICMS><PIS><PISNT><CST>04</CST></PISNT></PIS><COFINS>" _
& "<COFINSNT><CST>04</CST></COFINSNT></COFINS></imposto></det><total><vCFeLei12741>0.30</vCFeLei12741>" _
& "</total><pgto><MP><cMP>01</cMP><vMP>1.00</vMP></MP></pgto><infAdic><infCpl>Obrigado e volte sempre" _
& "</infCpl></infAdic></infCFe></CFe>"

i = TesteFimAFim(i, codA, dadosVenda)
ret = PointerToString(i)
val = Split(ret, "|")


Text1.Text = "Numero de sess�o: " & vbCrLf & val(0) & vbCrLf & vbCrLf
Text1.Text = Text1.Text & "Codigo: " & vbCrLf & val(1) & vbCrLf & vbCrLf
Text1.Text = Text1.Text & "Mensagem: " & vbCrLf & val(2) & vbCrLf & vbCrLf
Text1.Text = Text1.Text & "Cod. Ref. Mensagem SEFAZ: " & vbCrLf & val(3) & vbCrLf & vbCrLf
Text1.Text = Text1.Text & "Mensagem SEFAZ: " & vbCrLf & val(4) & vbCrLf & vbCrLf
Text1.Text = Text1.Text & "Arq. Base64: " & vbCrLf & val(5) & vbCrLf & vbCrLf
Text1.Text = Text1.Text & "TimerStamp: " & vbCrLf & val(6) & vbCrLf & vbCrLf
Text1.Text = Text1.Text & "Num. Doc. fiscal: " & vbCrLf & val(7) & vbCrLf & vbCrLf
Text1.Text = Text1.Text & "chave de consulta: " & vbCrLf & val(8)

End Sub

Private Sub Command6_Click()
If Text2.Text = "" Then
    MsgBox "Preencha o Codigo de ativa��o", vbCritical, "Erro"
Exit Sub
End If

Dim arrayDados() As String
codA = Text2.Text
i = GeraNumeroSessao
i = ConsultarStatusOperacional(i, codA)
ret = PointerToString(i)
arrayDados = Split(ret, "|")

ret = "Numero de sess�o: " & arrayDados(0) & vbCrLf _
& "Codigo de retorno: " & arrayDados(1) & vbCrLf _
& "Mensagem: " & arrayDados(2) & vbCrLf _
& "Cod. Ref. Mensagem SEFAZ: " & arrayDados(3) & vbCrLf _
& "Mensagem SEFAZ: " & arrayDados(4) & vbCrLf & vbCrLf _
& "Numero de serie: " & arrayDados(5) & vbCrLf _
& "Tipo de rede:    " & arrayDados(6) & vbCrLf _
& "IP:  " & arrayDados(7) & vbCrLf _
& "MAC: " & arrayDados(8) & vbCrLf _
& "Mascara: " & arrayDados(9) & vbCrLf _
& "Gateway: " & arrayDados(10) & vbCrLf _
& "DNS 1: " & arrayDados(11) & vbCrLf _
& "DNS 2: " & arrayDados(12) & vbCrLf _
& "Status LAN:  " & arrayDados(13) & vbCrLf _
& "Nivel da Bateria:    " & arrayDados(14) & vbCrLf _
& "Memoria de trabalho total:   " & arrayDados(15) & vbCrLf _
& "Memoria de trabalho usada:   " & arrayDados(16) & vbCrLf _
& "Data e hora atual:   " & arrayDados(17) & vbCrLf _
& "Vers�o do Firmeware: " & arrayDados(18) & vbCrLf _
& "Vers�o do layout da tabela:  " & arrayDados(19) & vbCrLf _
& "N�mero sequencial do �ltimo CF-e-SAT emitido:    " & arrayDados(20) & vbCrLf _
& "N�mero sequencial do primeiro CF-e-SAT armazenado:   " & arrayDados(21) & vbCrLf _
& "N�mero sequencial do �ltimo CF-e-SAT armazenado: " & arrayDados(22) & vbCrLf _
& "Data e hora da �ltima transmiss�o CF-e-SAT para SEFAZ:   " & arrayDados(23) & vbCrLf

ret = ret & "�ltima comunica��o com o SEFAZ:    " & arrayDados(24) & vbCrLf _
& "Data de emiss�o do certificado instalado:    " & arrayDados(25) & vbCrLf _
& "Data de vencimento do certificado:   " & arrayDados(26) & vbCrLf _
& "Estado de opera��o do SAT:   " & arrayDados(27)

Text1.Text = ret

End Sub


Private Sub Command7_Click()
If Text2.Text = "" Then
    MsgBox "Preencha o Codigo de ativa��o", vbCritical, "Erro"
Exit Sub
End If

Dim aux As Long
Dim val() As String
aux = i
i = GeraNumeroSessao
codA = Text2.Text
i = ConsultarNumeroSessao(i, codA, aux)
ret = PointerToString(i)
val = Split(ret, "|")

If Trim(val(0)) = "Erro ao tentar abrir a porta de comunicacao!!" Then
    Text1.Text = Text1.Text & "Erro: " & val(0)
    Exit Sub
Else
    Text1.Text = ""
    Text1.Text = Text1.Text & "Numero da Sess�o: " & val(0)
    Text1.Text = Text1.Text & vbCrLf & "Codigo de retorno: " & val(1)
    Text1.Text = Text1.Text & vbCrLf & "Mensagem: " & val(2)
    Text1.Text = Text1.Text & vbCrLf & "Codigo SEFAZ: " & val(3)
    Text1.Text = Text1.Text & vbCrLf & "Mensagem SEFAZ: " & val(4)
End If


End Sub

Private Sub Command8_Click()

Form2.Show

End Sub

Private Sub Command9_Click()
If Text2.Text = "" Then
    MsgBox "Preencha o Codigo de ativa��o", vbCritical, "Erro"
Exit Sub
End If
If Text3.Text = "" Then
    MsgBox "Preencha o CNPJ da Softhouse", vbCritical, "Erro"
Exit Sub
End If

If Text5.Text = "" Then
    MsgBox "Preencha o CNPJ do contribuinte", vbCritical, "Erro"
Exit Sub
End If

Dim cnpjSC As String
cnpjSC = Text3.Text
cnpjSC = cnpjSC & Text5.Text

i = GeraNumeroSessao
codA = Text2.Text

MsgBox cnpjSC

i = AssociarAssinatura(i, codA, cnpjSC, codA)
ret = PointerToString(i)
Text1.Text = ret
End Sub

